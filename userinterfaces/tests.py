from django.test import TestCase,Client
from django.test import SimpleTestCase
from django.urls import reverse, resolve
from accounts.models import Products
from . import views
from django.contrib.auth.models import User
# Create your tests here.




class TestUrls(SimpleTestCase):
    def test_fashion_url_resolves(self):
        url = reverse('fashion')
        self.assertEqual(resolve(url).func, views.fashion)

    def test_electronics_url_resolves(self):
        url = reverse('electronics')
        self.assertEqual(resolve(url).func, views.Electronics)

    def test_shop_url_resolves(self):
        url = reverse('shop')
        self.assertEqual(resolve(url).func, views.shop)

    def test_contactus_url_resolves(self):
        url = reverse('contactus')
        self.assertEqual(resolve(url).func, views.contactus)

    def test_aboutus_url_resolves(self):
        url = reverse('aboutus')
        self.assertEqual(resolve(url).func, views.About)

    
    def test_login_url_resolves(self):
        url = reverse('login')
        self.assertEqual(resolve(url).func, views.login_user)

    def test_logout_url_resolves(self):
        url = reverse('logout')
        self.assertEqual(resolve(url).func, views.logout_user)

    def test_customer_dash_url_resolves(self):
        url = reverse('customer_dash')
        self.assertEqual(resolve(url).func, views.customer_dash)

    def test_bdash_url_resolves(self):
        url = reverse('business_owners_dash')
        self.assertEqual(resolve(url).func, views.business_owners_dash)

    def test_welcome_url_resolves(self):
        url = reverse('welcome')
        self.assertEqual(resolve(url).func, views.welcome)

    def test_product_url_resolves(self):
        url = reverse('Product_details', args=[1])  # Assuming pk=1 for the test
        self.assertEqual(resolve(url).func, views.product_details)

class WelcomeViewTest(TestCase):
    def test_welcome_view(self):
        client = Client()
        response = client.get(reverse('welcome'))
        self.assertEqual(response.status_code, 200)

class ShopViewTest(TestCase):
    def test_shop_view(self):
        client = Client()
        response = client.get(reverse('shop'))
        self.assertEqual(response.status_code, 200)
    
class FashionViewTest(TestCase):
    def test_fashion_view(self):
        client = Client()
        response = client.get(reverse('fashion'))
        self.assertEqual(response.status_code, 200)
    

class ElectronicsViewTest(TestCase):
    def test_electronics_view(self):
        client = Client()
        response = client.get(reverse('electronics'))
        self.assertEqual(response.status_code, 200)
    

class CartViewTest(TestCase):
    def test_cart_view(self):
        client = Client()
        response = client.get(reverse('cart'))
        self.assertEqual(response.status_code, 200)
    

class CustomerDashViewTest(TestCase):
    def test_customer_dash_view(self):
        client = Client()
        response = client.get(reverse('customer_dash'))
        self.assertEqual(response.status_code, 200)
    

class BusinessOwnersDashViewTest(TestCase):
    def test_business_owners_dash_view(self):
        client = Client()
        response = client.get(reverse('business_owners_dash'))
        self.assertEqual(response.status_code, 200)
    

class LoginUserViewTest(TestCase):
    def test_login_user_view_get(self):
        client = Client()
        response = client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)
    

    def test_login_user_view_post(self):
        User.objects.create_user(username='testuser', password='password')
        client = Client()
        response = client.post(reverse('login'), {'username': 'testuser', 'password': 'password'})
        self.assertRedirects(response, '/')  
    
class LogoutUserViewTest(TestCase):
    def test_logout_user_view(self):
        client = Client()
        response = client.get(reverse('logout'))
        self.assertRedirects(response, '/')  
        

class AboutViewTest(TestCase):
    def test_about_view(self):
        client = Client()
        response = client.get(reverse('aboutus'))
        self.assertEqual(response.status_code, 200)
        

class ContactusViewTest(TestCase):
    def test_contactus_view(self):
        client = Client()
        response = client.get(reverse('contactus'))
        self.assertEqual(response.status_code, 200)
        

class ReviewViewTest(TestCase):
    def test_review_view(self):
        client = Client()
        response= client.get(reverse('review'))
        self.assertEqual(response.status_code, 200)