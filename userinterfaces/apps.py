from django.apps import AppConfig


class UserinterfacesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "userinterfaces"
