from django.urls import path
from . import views
# urls.py
from django.conf.urls import handler404

urlpatterns = [
    path('fashion/', views.fashion, name='fashion'),
    path('electronics/', views.Electronics, name='electronics'),
    path('shop/', views.shop, name='shop'),
    path('contactus/', views.contactus, name='contactus'),
    path('aboutus/', views.About, name='aboutus'),
    path('checkout/', views.checkout, name='checkout'),
    path('food&groceries/', views.food_groceries, name='food&groceries'),
    path('login/', views.login_user, name='login'),
    path('logout/', views.logout_user, name='logout'),
    path('review/', views.review, name='review'),
    path('profile/', views.profile, name='profile'),
    path('customer_dash/', views.customer_dash, name='customer_dash'),
    path('bdash/', views.business_owners_dash, name='business_owners_dash'),
    path('', views.welcome, name='welcome'),
    path('product/<int:pk>/', views.product_details, name='Product_details'),
]
handler404:views.custom_404_page






