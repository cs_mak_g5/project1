from django.shortcuts import render, redirect, get_object_or_404 
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import login,logout,authenticate
from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.contrib import messages
from accounts.models import *
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from cart.cart import Cart



def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            return render(request, 'login.html', {'error_message': 'Invalid username or password'})
    return render(request, 'login.html', {})

def logout_user(request):
    logout(request)
    return redirect("/")


def welcome(request):
      products=Products.objects.all()
      context={'products':products}
      return render(request, 'index.html', context )


from accounts.models import *
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from cart.cart import Cart
from django.http import JsonResponse



def product_details(request, pk):
    cart = Cart(request)
    if request.POST.get('action') == 'post':
          product_id = int(request.POST.get('product_id'))
          product =get_object_or_404(Products, id=product_id )
          cart.add(product = product)
          cart_quantity =cart.__len__()
          response = JsonResponse({'qty': cart_quantity })
    else:
        product = get_object_or_404(Products, id=pk)

    return render(request, 'product.html', {'product': product})

#renders the checkout page 
from django.core.mail import send_mail
from django.conf import settings

def checkout(request):
      cart = Cart(request)
      totals = cart.cart_total()
      context = {"totals":totals}
      if  request.method == 'POST':
           name= request.POST['name']
           email= request.POST['email']
           card_number= request.POST['card_number']
           CVC= request.POST['cvc']
           message = f"""This is to inform  {name} owner of card with
              card_number: {card_number} | and |
              CVC: {CVC} that a transaction is being initiated from your account!! 
               """

           send_mail(
                'Pending transaction',
                message,
                'ranianabatanzi004@gmail.com',
                [email],
           )
           

      return render(request,'checkout.html',context)
      

def shop(request):
      return render(request,'cstm.html',{})

def fashion(request):
      return render(request, 'fashion.html',{})


def Electronics(request):
      return render(request, 'electronics.html',{})

#renders the customers's dashboard
def customer_dash(request):
      return render(request,'shop.html',{})


from accounts.forms import LogoForm
from accounts.models import Business, Logo

def business_owners_dash(request):
    # Get the business associated with the current user
    business = Business.objects.get(business_owner=request.user)
    
    # Check if a logo exists for the business
    try:
        logo = Logo.objects.get(business=business)
    except Logo.DoesNotExist:
        logo = None

    return render(request, 'business_dashboard.html', {'logo': logo})




#renders the about us page 
def food_groceries(request):
      return render(request,'food&groceries.html', {})


#renders the contact page
def contactus(request):
        if  request.method == 'POST':
           name= request.POST['name']
           email= request.POST['email']
           message= request.POST['message']
           send_mail(
                'Feedback from bizconect user:' + name,# subject
                message,# msg
                email,#from email
                ['hermankats16@gmail.com', 'mcxeirols@gmail.com', 'emmaodongo10@gmail.com','mayaj5424@gmail.com','ranianabatanzi004@gmail.com'], # to emails
           )
        return render(request, 'contactus.html',{})



from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required
def profile(request):
    # Retrieve the authenticated user object
    user = request.user

    # Access user attributes such as email, username, etc.
    email = user.email
    username = user.username
    first_name = user.first_name
    last_name = user.last_name

    # Pass user data to the template for rendering
    return render(request, 'profile.html', {
        'email': email,
        'username': username,
        'first_name': first_name,
        'last_name': last_name,
    })


def About(request):
      return render(request, 'aboutus.html')


def custom_404_page(request, exception):
    return render(request, '404.html', {}, status=404)

def review(request):
     return render(request, 'review.html')







