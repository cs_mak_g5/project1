

function toggleSidebar() {
    var sidebar = document.getElementById('sidebar');
    var content = document.getElementById('content');

    if (sidebar.style.transform === 'translateX(-250px)') {
        sidebar.style.transform = 'translateX(0)';
        content.style.marginLeft = '250px';
    } else {
        sidebar.style.transform = 'translateX(-250px)';
        content.style.marginLeft = '0';
    }
}