# settings_prod.py

from .settings import *
import django_heroku
import dj_database_url
from decouple import config

DEBUG = True

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
django_heroku.settings(
    locals()
    
)


# DATABASES = {
#      "default": {
#          "ENGINE":"django.db.backends.postgresql",
#          "NAME":"bizconect",
#          "USER":"herman",
#          "PASSWORD":"12345678",
#          "HOST":"bizconect-db1.cdwy40goet8l.eu-north-1.rds.amazonaws.com",
#          'PORT':"5432",
#      }
#  }
