## Name
E-commerce_platform

## Description
it is a platform where business owners and customers can interact, make online orders,bookings
and delivery concerning business opportunities
On this platform business owners and customers can make accounts
business owners can show case their products  and customers make choices of the products depending on their preferences


## Usage
#Business owners:

-they can make business accounts

-they can create an inventory of all their products

-can view order and booking requests from different customers 

-can be able to update their inventory 

#Customers:

-can create personalised accounts

-can view all products under their preference 

-can make online orders and bookings from business owners

## Support

Help can be obtained from the following contacts

1.hermankats16@gmail.com 

2.ranianabatanzi004@gmail.com 

3.emmaodongo10@gmail.com 

4.mayaj5424@gmail.com 

5.mcxeirols@gmail.com 
## Roadmap
More releases and updates to the project will be availed in the near future

## Contributions
For any contributions pleasse follow the steps below

#step1
-contact any of the Authors 

#step2 
-create a fork and make contributions 

#step3
-before making amerge request contact any of the authors for more guidance. 

## Authors and acknowledgment
Appreciation goes to the following individuals for making this project a success

1.herman Katende

2.samuel Muwanguzi

3.emmanuel Odongo

4.Rania Nabatanzi

5.Janet Mayanja


## License
This project is an open source project 

## Project status
This project is still under development 
