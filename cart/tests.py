from django.test import TestCase
from django.urls import reverse

class CartURLsTest(TestCase):
    def test_cart_url(self):
        url = reverse('cart')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        # Add more assertions as needed

    def test_cart_add_url(self):
        url = reverse('cart_add')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        # Add more assertions as needed

    def test_cart_delete_url(self):
        url = reverse('cart_delete')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        # Add more assertions as needed

    def test_cart_update_url(self):
        url = reverse('cart_update')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        # Add more assertions as needed

    def test_product_cart_url(self):
        url = reverse('cart', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        # Add more assertions as needed

    def test_add_to_cart_url(self):
        url = reverse('add_to_cart', kwargs={'product_identifier': 1})
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        # Add more assertions as needed
from django.test import TestCase, RequestFactory
from django.urls import reverse
from .views import cart, cart_add, cart_delete
from accounts.models import Products

class CartViewsTest(TestCase):
    def setUp(self):
        # Set up the request factory
        self.factory = RequestFactory()

    def test_cart_view(self):
        # Create a request
        request = self.factory.get(reverse('cart'))
        # Call the view function
        response = cart(request)
        # Check if the response is successful
        self.assertEqual(response.status_code, 200)

    def test_cart_add_view(self):
        # Create a test product
        product = Products.objects.create(name='Test Product', price=10)
        # Create a request with POST data
        request = self.factory.post(reverse('cart_add'), {'action': 'post', 'product_id': product.id})
        # Call the view function
        response = cart_add(request)
        # Check if the response is successful
        self.assertEqual(response.status_code, 200)
        # Check if the product is added to the cart
        self.assertEqual(response.json()['qty'], 1)  # Assuming one quantity is added

    def test_cart_delete_view(self):
        # Create a test product
        product = Products.objects.create(name='Test Product', price=10)
        # Add the product to the cart
        request = self.factory.post(reverse('cart_add'), {'action': 'post', 'product_id': product.id})
        cart_add(request)  # Add the product to the cart
        # Create a request with POST data
        request = self.factory.post(reverse('cart_delete'), {'action': 'post', 'product_id': product.id})
        # Call the view function
        response = cart_delete(request)
        # Check if the response is successful
        self.assertEqual(response.status_code, 200)
        # Check if the product is deleted from the cart
        self.assertEqual(response.json()['product'], str(product.id))
