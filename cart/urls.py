from django.urls import path
from . import views
# urls.py




urlpatterns = [
    path('cart/', views.cart, name='cart'),
    path('add/', views.cart_add, name='cart_add'),
    path('delete/', views.cart_delete, name='cart_delete'),
    path('product/<int:pk>/cart/', views.cart, name='cart'),
    path('add-to-cart/<int:product_identifier>/', views.cart_add, name='add_to_cart')
]