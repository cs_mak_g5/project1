
from django.shortcuts import render, redirect, get_object_or_404 
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import login,logout,authenticate
# Create your views here.
from django.contrib import messages
from accounts.models import *
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .cart import Cart
from django.http import JsonResponse

# Create your views here.

#cart functionality
def cart(request):
    cart = Cart(request)
    cart_products = cart.get_prods()
    quantities = cart.get_quants
    totals = cart.cart_total()
    context = {'cart_products': cart_products, "quantities":quantities, "totals":totals}
    return render(request, 'cart.html', context)



def cart_add(request):
     cart = Cart(request)
     if request.POST.get('action') == 'post':
          product_id = int(request.POST.get('product_id'))
          product =get_object_or_404(Products, id=product_id )
          cart.add(product = product)
          cart_quantity =cart.__len__()
          response = JsonResponse({'qty': cart_quantity })
          return response


def cart_delete(request):
     cart =Cart(request)
     if request.POST.get('action')== 'post':
          product_id = int(request.POST.get('product_id'))
          cart.delete(product=product_id)
          response =JsonResponse({'product': product_id})
          return response



