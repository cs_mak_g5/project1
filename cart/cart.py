from accounts.models import *


class Cart():
    def __init__(self,request):
        self.session =request.session

        #get sesion key if it exists

        cart= self.session.get('session_key')

        #if the user is new, create one 
        if 'session_key' not in request.session:
            cart= self.session['session_key'] = {}

        # making cart available on all pages
        self.cart =cart
    def add(self, product):
        product_id= str(product.id)
        #product_qty= str(quantity)
        if product_id in self.cart:
            pass
        else: 
            self.cart[product_id] = {'Price': str(product.price)}
            #self.cart[product_id] = int(product_qty)
        self.session.modified = True
    
    def __len__(self):
        return len(self.cart)
    

    def get_prods(self):  
        product_ids = self.cart.keys()
        products = Products.objects.filter(id__in = product_ids)
        return products
    
    def get_quants(self):
        quantities=self.cart
        return quantities
    def delete(self,product):
        product_id = str(product)
        if product_id in  self.cart:
            del self.cart[product_id]
        self.session.modified =True

  
    def cart_total(self):
        product_ids = self.cart.keys()
        products = Products.objects.filter(id__in=product_ids)
        total = 0
        for product in products:
            quantity_dict = self.cart.get(str(product.id))
            quantity = quantity_dict.get('price', 1)  # Get the quantity value or default to 0
            try:
                quantity = int(quantity)  # Convert quantity to integer
                total += float(product.price) * quantity  # Convert product price to float
            except (ValueError, TypeError):
                # Handle cases where quantity or product price cannot be converted to numeric types
                pass
        return total

