from .cart import Cart

#create context processor to make cart work on all pages
def cart(request):
    #return the defailt data from the cart
    return {'cart': Cart(request)}