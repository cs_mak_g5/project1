from django.shortcuts import render,redirect
# Create your views here.
from .models import *
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .forms import SignUpForm
from django.contrib import messages
from .models import Category 
from .forms import ProductForm
from django.contrib.auth.decorators import login_required
from .forms import BusinessRegistrationForm
from .forms import *
def register_business(request):
    if request.method == 'POST':
        form = BusinessRegistrationForm(request.POST)
        if form.is_valid():
            business = form.save(commit=False)
            business.business_owner = request.user
            business.save()
        
            return redirect('business_owners_dash')  
    else:
        form = BusinessRegistrationForm()

    if not request.user.is_authenticated:
        messages.info(request, "Please sign up or log in as a user to register your business.")
        return render(request, 'businessform.html', {'form': form})
    return render(request, 'businessform.html', {'form': form})

def product_upload(request):
    form = ProductForm()
    products = Products.objects.all()
    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('upload')

    context = {'form': form, 'products': products}
    return render(request, 'upload.html', context)



def category(request, catgry):
    #replace hyphen with spaces
    catgry = catgry.replace('-', ' ')
    try:
        category = Category.objects.get(name=catgry)
        product = Products.objects.filter(category=category)
        return render(request, 'category.html',{'product':product, 'category':category})
    except:
        messages.success(request,("that category does not exit..."))
        return redirect('/')



def search(request):
    query = request.GET.get('query')
    products = Products.objects.all()  # Retrieve all products by default
    
    if query:
        # Perform search query based on the 'Product_name' field
        products = Products.objects.filter(category__name__icontains=query)

    return render(request, 'categry.html', {'products': products, 'query': query})



def signup(request):
    if request.method == 'POST':   
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login/')
         
    else:
        form =SignUpForm()
    return render(request,'signup.html', {'form':form})

from .forms import LogoForm  # Import your LogoForm
from .forms import LogoForm
from .models import Business

def logo(request):
    if request.method == 'POST':
        form = LogoForm(request.POST, request.FILES)
        if form.is_valid():
            logo_instance = form.save(commit=False)
            # Retrieve the business associated with the current user
            business = Business.objects.get(business_owner=request.user)
            logo_instance.business = business
            logo_instance.save()
            return redirect('/')
    else:
        form = LogoForm()

    return render(request, 'logo.html', {'form': form})



#jkfk
# views.py



# # views.py

# from django.contrib.auth.decorators import login_required

# # def profile(request):
# #     # Profile view logic...


# #from django.shortcuts import render
# # #from .models import CustomerProfile


# @login_required
# def profile(request):
#     user = request.user
#     profile = CustomerProfile.objects.get_or_create(user=user)
#     return render(request, 'profile.html', {'profile': profile})
# from django.shortcuts import render
# from .models import Order
# from .email_utils import send_order_confirmation_email, send_order_notification_email

# def place_order(request):
#     # Your order placement logic here
#     # Example: Creating a new order object
#     order = Order.objects.create(customer=request.user)

#     # Send order confirmation email to the customer
#     send_order_confirmation_email(order)

#     # Send order notification email to the business owner
#     send_order_notification_email(order)

#     # Return appropriate response
#     return render(request, 'order_placed.html', {'order': order})
