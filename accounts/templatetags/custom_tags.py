from django import template
from accounts.models import Business

register = template.Library()

@register.filter
def is_business_owner(user):
    return Business.objects.filter(business_owner=user).exists()
