# Generated by Django 5.0.1 on 2024-03-03 22:57

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Business_Owners_details",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("Name", models.TextField(max_length=30)),
                ("Business_description", models.TextField()),
                ("Contact", models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name="Products",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("Product_name", models.TextField()),
                ("Product_image", models.ImageField(upload_to="")),
                ("Product_description", models.TextField()),
                ("Product_details", models.TextField()),
            ],
        ),
    ]
