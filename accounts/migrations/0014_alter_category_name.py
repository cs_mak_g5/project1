# Generated by Django 5.0.3 on 2024-03-23 13:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0013_category_products_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(choices=[('fashion', 'fashion'), ('electronics', 'electronics'), ('electronics', 'electronics'), ('home&living', 'home&living'), ('health&beauty', 'health&beauty')], default=1, max_length=200),
        ),
    ]
