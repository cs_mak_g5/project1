# Generated by Django 5.0.3 on 2024-03-19 20:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_rename_product_detail_products_product_details'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, null=True)),
                ('phone', models.CharField(max_length=100, null=True)),
                ('email', models.EmailField(max_length=100, null=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.AlterField(
            model_name='businesses',
            name='Business_Name',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='businesses',
            name='Contact',
            field=models.TextField(null=True),
        ),
    ]
