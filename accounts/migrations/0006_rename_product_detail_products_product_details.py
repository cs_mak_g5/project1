# Generated by Django 5.0.3 on 2024-03-14 07:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_rename_product_details_products_product_detail_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='products',
            old_name='Product_detail',
            new_name='Product_details',
        ),
    ]
