from django.test import TestCase
from .models import *
from .views import *
# Create your tests here.
from django.test import TestCase, Client
from django.urls import reverse
from .forms import ProductForm
from django.test import TestCase, RequestFactory
from django.urls import reverse
from django.test import TestCase, RequestFactory
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.messages.storage.fallback import FallbackStorage
from .forms import BusinessRegistrationForm
from django.test import TestCase
from django.test import TestCase
from django.test import TestCase
from .forms import SignUpForm, ProductForm, BusinessRegistrationForm
from django.contrib.auth.models import User
from django.test import TestCase, RequestFactory
from django.urls import reverse
from .forms import SignUpForm
from django.test import TestCase
from django.test import TestCase
from django.test import TestCase, RequestFactory
from django.urls import reverse
from django.test import TestCase
from django.contrib.auth.models import User
from .models import Customer, Business, Logo, Category, Products, Order, OrderItem
from .forms import SignUpForm, BusinessRegistrationForm

class CustomerModelTest(TestCase):
    def test_customer_model(self):
        user = User.objects.create(username='john_doe', email='john@example.com')
        customer = Customer.objects.create(user=user, phone='1234567890', email='john@example.com')
        self.assertEqual(customer.user.username, 'john_doe')
        self.assertEqual(customer.email, 'john@example.com')

class BusinessModelTest(TestCase):
    def test_business_model(self):
        business = Business.objects.create(
            business_owner='Jane Doe',
            business_name='Doe Enterprises',
            business_description='A clothing store specializing in casual wear.',
            email='jane@example.com',
            contact_number='1234567890',
            location='New York',
        )
        self.assertEqual(business.business_owner, 'Jane Doe')
        self.assertEqual(business.business_name, 'Doe Enterprises')

class LogoModelTest(TestCase):
    def test_logo_model(self):
        business = Business.objects.create(
            business_owner='Jane Doe',
            business_name='Doe Enterprises',
            business_description='A clothing store specializing in casual wear.',
            email='jane@example.com',
            contact_number='1234567890',
            location='New York',
        )
        logo = Logo.objects.create(business=business, image='logo.png')
        self.assertEqual(logo.business.business_name, 'Doe Enterprises')

class CategoryModelTest(TestCase):
    def test_category_model(self):
        category = Category.objects.create(name='Fashion')
        self.assertEqual(category.name, 'Fashion')

class ProductsModelTest(TestCase):
    def test_products_model(self):
        category = Category.objects.create(name='Fashion')
        product = Products.objects.create(
            Product_name='T-Shirt',
            category=category,
            Product_image='tshirt.png',
            price=29.99,
            Product_description='Comfortable cotton t-shirt.',
            Product_details='Available in multiple sizes and colors.',
            is_sale=False,
            sale_price=0,
        )
        self.assertEqual(product.Product_name, 'T-Shirt')

class OrderModelTest(TestCase):
    def test_order_model(self):
        user = User.objects.create(username='john_doe', email='john@example.com')
        customer = Customer.objects.create(user=user, phone='1234567890', email='john@example.com')
        order = Order.objects.create(customer=customer, address='123 Main St', quantity=2)
        self.assertEqual(order.customer.user.username, 'john_doe')
        self.assertEqual(order.address, '123 Main St')

class OrderItemModelTest(TestCase):
    def test_order_item_model(self):
        category = Category.objects.create(name='Fashion')
        product = Products.objects.create(
            Product_name='T-Shirt',
            category=category,
            Product_image='tshirt.png',
            price=29.99,
            Product_description='Comfortable cotton t-shirt.',
            Product_details='Available in multiple sizes and colors.',
            is_sale=False,
            sale_price=0,
        )
        user = User.objects.create(username='john_doe', email='john@example.com')
        customer = Customer.objects.create(user=user, phone='1234567890', email='john@example.com')
        order = Order.objects.create(customer=customer, address='123 Main St', quantity=2)
        order_item = OrderItem.objects.create(product=product, order=order, quantity=2)
        self.assertEqual(order_item.product.Product_name, 'T-Shirt')

class SignUpFormTest(TestCase):
    def test_signup_form_valid(self):
        form_data = {
            'username': 'john_doe',
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'john@example.com',
            'password1': 'P@ssw0rd!',
            'password2': 'P@ssw0rd!',
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_signup_form_invalid(self):
        form_data = {
            'username': 'john_doe',
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'invalid_email',  # Invalid email format
            'password1': 'P@ssw0rd!',
            'password2': 'DifferentPassword',  # Passwords don't match
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())
class BusinessRegistrationFormTest(TestCase):
    def test_business_registration_form_valid(self):
        form_data = {
            'business_owner': 'Jane Doe',
            'business_name': 'Doe Enterprises',
            'email': 'jane@example.com',
            'contact_number': '1234567890',
            'location': 'New York',
            'type_of_business': 'Fashion',
            'business_description': 'A clothing store specializing in casual wear.',  # Add business description
        }
        form = BusinessRegistrationForm(data=form_data)
        self.assertTrue(form.is_valid())


    def test_business_registration_form_invalid(self):
        form_data = {
            'business_owner': '',  # Business owner name missing
            'business_name': 'Doe Enterprises',
            'email': 'invalid_email',  # Invalid email format
            'contact_number': '1234567890',
            'location': 'New York',
            'type_of_business': '',  # Type of business not selected
            'description': '',  # Description missing
        }
        form = BusinessRegistrationForm(data=form_data)
        self.assertFalse(form.is_valid())

class URLTests(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='testuser')

    def test_business_registration_url(self):
        url = reverse('business_Registration')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'businessform.html')

    def test_signup_url(self):
        url = reverse('signup')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')

    def test_category_url(self):
        category_name = 'test-category'
        url = reverse('category', kwargs={'catgry': category_name})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)  # Assuming it redirects for an invalid category

    def test_search_url(self):
        url = reverse('search')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'categry.html')

    def test_upload_url(self):
        url = reverse('upload')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'upload.html')

    def test_login_url(self):
        url = reverse('login')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)  # Assuming it's a login page

    def test_logo_url(self):
        url = reverse('logo')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'logo.html')

from django.test import TestCase
from django.contrib.auth.models import User
from .forms import SignUpForm, ProductForm, BusinessRegistrationForm, LogoForm
from django.test import TestCase
from django.contrib.auth.models import User
from .forms import SignUpForm, ProductForm, BusinessRegistrationForm, LogoForm

class SignUpFormTest(TestCase):
    def test_valid_form(self):
        form_data = {
            'username': 'testuser',
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'test@example.com',
            'password1': 'testpassword123',
            'password2': 'testpassword123',
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form_data = {}  # Empty data
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid())


class ProductFormTest(TestCase):
    def test_valid_form(self):
        form_data = {
            'name': 'Test Product',
            'description': 'Test description',
            # Add other required fields according to your model
        }
        form = ProductForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form_data = {}  # Empty data
        form = ProductForm(data=form_data)
        self.assertFalse(form.is_valid())


class BusinessRegistrationFormTest(TestCase):
    def test_valid_form(self):
        form_data = {
            'business_owner': User.objects.create(username='testuser'),
            'business_name': 'Test Business',
            'business_description': 'Test description',
            'email': 'test@example.com',
            'contact_number': '1234567890',
            'location': 'Test location',
        }
        form = BusinessRegistrationForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form_data = {}  # Empty data
        form = BusinessRegistrationForm(data=form_data)
        self.assertFalse(form.is_valid())


class LogoFormTest(TestCase):
    def test_valid_form(self):
        # Create a Business instance to associate with the Logo
        business = Business.objects.create(
            business_owner=User.objects.create(username='testuser'),
            business_name='Test Business',
            business_description='Test description',
            email='test@example.com',
            contact_number='1234567890',
            location='Test location',
        )
        form_data = {'image': 'test_image.jpg'}  # Add image field data
        form = LogoForm(data=form_data)
        form.instance.business = business  # Associate the form with the business
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form_data = {}  # Empty data
        form = LogoForm(data=form_data)
        self.assertFalse(form.is_valid())



# from django.test import TestCase, RequestFactory
# from django.urls import reverse
# from django.contrib.auth.models import User
# from django.contrib.messages.storage.fallback import FallbackStorage
# from .models import Products, Category
# from .views import register_business, product_upload, category, search, signup, logo

# class RegisterBusinessViewTest(TestCase):
#     def setUp(self):
#         self.factory = RequestFactory()
    
#     def test_register_business_view_get(self):
#         url = reverse('business_Registration')
#         request = self.factory.get(url)
#         response = register_business(request)
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'businessform.html')

#     def test_register_business_view_post(self):
#         user = User.objects.create(username='testuser')
#         data = {'business_name': 'Test Business'}
#         url = reverse('business_Registration')
#         request = self.factory.post(url, data)
#         request.user = user
#         response = register_business(request)
#         self.assertEqual(response.status_code, 302)
#         self.assertTrue(Category.objects.filter(name='Test Business').exists())


# class ProductUploadViewTest(TestCase):
#     def setUp(self):
#         self.factory = RequestFactory()
    
#     def test_product_upload_view_get(self):
#         url = reverse('upload')
#         request = self.factory.get(url)
#         response = product_upload(request)
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'upload.html')

#     # Add other test methods for POST requests if needed


# class CategoryViewTest(TestCase):
#     def setUp(self):
#         self.factory = RequestFactory()
#         self.category = Category.objects.create(name='electronics')
#         self.product = Products.objects.create(Product_name='Test Product', category=self.category)

#     def test_category_view(self):
#         url = reverse('category', kwargs={'catgry': 'electronics'})
#         request = self.factory.get(url)
#         response = category(request, 'electronics')
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'category.html')

#     # Add test methods for invalid category or other scenarios if needed


# class SearchViewTest(TestCase):
#     def setUp(self):
#         self.factory = RequestFactory()
#         self.product = Products.objects.create(Product_name='Test Product', category=Category.objects.create(name='electronics'))

#     def test_search_view(self):
#         url = reverse('search') + '?query=electronics'
#         request = self.factory.get(url)
#         response = search(request)
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'categry.html')

#     # Add other test methods for search view if needed


# class SignupViewTest(TestCase):
#     def setUp(self):
#         self.factory = RequestFactory()

#     def test_signup_view_get(self):
#         url = reverse('signup')
#         request = self.factory.get(url)
#         response = signup(request)
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'signup.html')

#     # Add test methods for POST requests if needed


# class LogoViewTest(TestCase):
#     def setUp(self):
#         self.factory = RequestFactory()
#         self.user = User.objects.create(username='testuser')

#     def test_logo_view_get(self):
#         url = reverse('logo')
#         request = self.factory.get(url)
#         request.user = self.user
#         response = logo(request)
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'logo.html')

#     # Add test methods for POST requests if needed
