from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import  forms  
from django.forms import ModelForm
from .models import *

class SignUpForm(UserCreationForm):
	email = forms.EmailField(label="", widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Email Address'}))
	first_name = forms.CharField(label="", max_length=100, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'First Name'}))
	last_name = forms.CharField(label="", max_length=100, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Last Name'}))
	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')




class ProductForm(forms.ModelForm):
    class Meta:
        model = Products
        fields = '__all__'





class BusinessRegistrationForm(forms.ModelForm):
    class Meta:
        model = Business
        fields = ['business_owner', 'business_name', 'business_description', 'email', 'contact_number', 'location']


class LogoForm(forms.ModelForm):
    class Meta:
        model = Logo
        fields = ['image']
