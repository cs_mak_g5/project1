# from django.core.mail import send_mail
# from django.template.loader import render_to_string
# from django.utils.html import strip_tags

# def send_order_confirmation_email(order):
#     subject = 'Order Confirmation'
#     template = 'order_confirmation_email.html'
#     context = {'order': order}  # Pass relevant data to the email template

#     html_message = render_to_string(template, context)
#     plain_message = strip_tags(html_message)  # Convert HTML to plain text for compatibility

#     send_mail(
#         subject,
#         plain_message,
#         'your@example.com',  # Sender email address
#         [order.customer.email],  # Customer's email address
#         html_message=html_message,
#     )
# def send_order_notification_email(order):
#     subject = 'New Order Notification'
#     template = 'order_notification_email.html'
#     context = {'order': order}  # Pass relevant data to the email template

#     html_message = render_to_string(template, context)
#     plain_message = strip_tags(html_message)  # Convert HTML to plain text for compatibility

#     send_mail(
#         subject,
#         plain_message,
#         'your@example.com',  # Sender email address
#         ['business_owner@example.com'],  # Business owner's email address
#         html_message=html_message,
#     )
# def send_appointment_reminder(appointment):
#     subject = 'Appointment Reminder'
#     template = 'appointment_reminder_email.html'
#     context = {'appointment': appointment}  # Pass relevant data to the email template

#     html_message = render_to_string(template, context)
#     plain_message = strip_tags(html_message)  # Convert HTML to plain text for compatibility

#     send_mail(
#         subject,
#         plain_message,
#         'your@example.com',  # Sender email address
#         [appointment.customer.email],  # Customer's email address
#         html_message=html_message,
#     )
# def send_account_activation_email(user):
#     subject = 'Account Activation'
#     template = 'account_activation_email.html'
#     context = {'user': user}  # Pass relevant data to the email template

#     html_message = render_to_string(template, context)
#     plain_message = strip_tags(html_message)  # Convert HTML to plain text for compatibility

#     send_mail(
#         subject,
#         plain_message,
#         'your@example.com',  # Sender email address
#         [user.email],  # User's email address
#         html_message=html_message,
#     )
# from django.contrib.auth.tokens import default_token_generator
# from django.utils.encoding import force_bytes
# from django.utils.http import urlsafe_base64_encode

# def send_password_reset_email(user):
#     subject = 'Password Reset'
#     template = 'password_reset_email.html'
#     context = {
#         'user': user,
#         'uid': urlsafe_base64_encode(force_bytes(user.pk)),
#         'token': default_token_generator.make_token(user),
#     }

#     html_message = render_to_string(template, context)
#     plain_message = strip_tags(html_message)  # Convert HTML to plain text for compatibility

#     send_mail(
#         subject,
#         plain_message,
#         'your@example.com',  # Sender email address
#         [user.email],  # User's email address
#         html_message=html_message,
#     )




# ########
# from django.core.mail import send_mail

# send_mail(
#     'Subject here',
#     'Here is the message.',
#     'from@example.com',
#     ['to@example.com'],
#     fail_silently=False,
# )
