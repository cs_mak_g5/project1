from django.urls import path
from . import views
from userinterfaces.views import login_user 

urlpatterns = [
    path("business_Registration",views.register_business,name='business_Registration'),
    path('signup/',views.signup,name='signup'),
    path('category/<str:catgry>',views.category,name='category'),
    path('search/',views.search, name='search'),
    path('upload/', views.product_upload, name='upload'),
    path('signup/login/', login_user, name='login'),
    path('logo/', views.logo, name='logo'),
]
