from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    phone = models.CharField(max_length=100, null=True)
    email = models.EmailField(max_length=100, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return str(self.user)


class Business(models.Model):
    business_owner = models.CharField(max_length=200, null=True)
    business_name = models.CharField(max_length=200, null=True)
    business_description = models.TextField()
    email = models.EmailField()
    contact_number = models.CharField(max_length=20)
    location = models.CharField(max_length=200)
    

    def __str__(self):
        return self.business_name

class Logo(models.Model):
    business = models.OneToOneField(Business, on_delete=models.CASCADE, related_name='logo', null=True)
    image = models.ImageField(upload_to='logos/')

    def __str__(self):
        return f"Logo for {self.business.business_name}"


class Category(models.Model):
    CATEGORIES=[
        ('fashion','fashion'),
        ('electronics','electronics'),
        ('home&living','home&living'),
        ('health&beauty','health&beauty'),
    ]
    name =models.CharField(max_length=200, choices=CATEGORIES, default=1 )
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural ='categories'




class Products(models.Model):
    CATEGORIES=[
        ('fashion','fashion'),
        ('electronics','electronics'),
        ('electronics','electronics'),
        ('home&living','home&living'),
        ('health&beauty','health&beauty'),
    ]

    Product_name=models.CharField(max_length=100)
    category =models.ForeignKey(Category,on_delete=models.SET_NULL, null=True)
    Product_image=models.ImageField(upload_to="img")
    price = models.FloatField(null=True)
    Product_description=models.TextField()
    Product_details=models.TextField()
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    is_sale =models.BooleanField(default=False)
    sale_price =models.DecimalField(default=0, decimal_places=2, max_digits=6)


    def __str__(self):
        return self.Product_name



class Order(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    customer = models.ForeignKey(Customer,null=True,on_delete=models.SET_NULL )
    address = models.CharField(max_length=100, default=" ")
    quantity =models.IntegerField(default=1)
    def __str__(self):
        return str(self.id)

class OrderItem(models.Model):
    product = models.ForeignKey(Products, on_delete=models.SET_NULL,blank=True, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL,blank=True, null=True)
    quantity = models.IntegerField(default=1)






    # models.py
